package Model;

public class ConversorTemperatura {
	
	 public double celsiusPraFahrenheit (double valorCelsius){
	        return 9*valorCelsius/5+32;
	    }    
	    
	    public double FahrenheitPraCelsius (double valorFahrenheit){
	        return 5*(valorFahrenheit - 32)/9;
	    }
	    
	    public double FahrenheitPraKelvin (double valorFahrenheit){
			return 5*(valorFahrenheit - 32)/9 + 273;
		}
		
		public double KelvinPraFahrenheit (double valorKelvin){
			return 1.8*(valorKelvin - 273) + 32;
		}
	    
	    public double CelsiusPraKelvin (double valorCelsius){
			return valorCelsius + 273;
		}
		
		public double KelvinPraCelsius (double valorKelvin){
			return valorKelvin - 273;
		}
		

}
