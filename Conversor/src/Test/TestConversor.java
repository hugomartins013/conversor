package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.ConversorTemperatura;

public class TestConversor {
	
	private ConversorTemperatura meuConversor;
	
	@Before
	public void setUp()throws Exception{
		meuConversor = new ConversorTemperatura();
	}

	@Test
	public void testCelsiusPraFahrenheitComValorCelsiusDouble() {
		assertEquals(104.0, meuConversor.celsiusPraFahrenheit(40.0), 0.01);
		
	}
	
	@Test
	public void testFahrenheitPraCelsiusComValorFahrenheitDouble() {
		assertEquals(40.0, meuConversor.FahrenheitPraCelsius(104.0), 0.01);
		
	}
	
	
	@Test
	public void testFahrenheitPraKelvinComValorFahrenheitDouble() {
		assertEquals(373 , meuConversor.FahrenheitPraKelvin(212), 0.01);
		
	}
	
	@Test
	public void testKelvinPraFahrenheitComValorKelvinDouble() {
		assertEquals(212 , meuConversor.KelvinPraFahrenheit(373), 0.01);
		
	}
	
	@Test
	public void testCelsiusPraKelvinComValorCelsiusDouble() {
		assertEquals(273 , meuConversor.CelsiusPraKelvin(0), 0.01);
		
	}
	
	@Test
	public void testKelvinPraCelsiusComValorKelvinDouble() {
		assertEquals(0 , meuConversor.KelvinPraCelsius(273), 0.01);
		
	}

	private void assertEquals(double e, double kelvinPraFahrenheit, double d) {
		// TODO Auto-generated method stub
		
	}

}
